/**
 * This `meta` object is used to define the necessary metadata for inferencer to work with.
 *
 * They will be used to infer the fields of the response of the data provider.
 * Also they will be included in the generated code, making them easily editable after you generate the boilerplate code for your resource.
 */
// Somewhat useful:
// https://refine.dev/docs/packages/documentation/inferencer/#usage-with-graphql-backends-and-meta-values
export const inferencerPredefinedMeta = {
  users: {
    getList: {
      fields: [
        "id",
        "username",
        "email",
        "zipCode"
      ]
    }
  },
  representatives: {
    getList: {
      fields: [
        "id",
        "name",
        "firstname",
        "lastname",
        "partyName",
        "districtName"
      ]
    },
    getOne: {
      fields: [
        "id",
        "name",
        "firstname",
        "created_at",
        "updated_at",
        "lastname",
        "partyName",
        "districtName"
      ]
    }
  },
  postalCodes: {
    getList: {
      fields: [
        "id",
        "code",
        "province",
        "city",
        {
          representatives: ["id", "representativeId", "postalCodeId"]
          // Table renders, but relationship doesn't render.
          // Not sure how to tell Inferencer that the "id" in this field is to the
          // representatives_postalCodes table and not the representatives table.
          // This may be something Inferencer can't do. Probably this interface
          // needs to be custom coded. Though it's possible the "fieldTransformer"
          // property of inferencer might be useable here:
          // https://refine.dev/docs/packages/documentation/inferencer/#modifying-the-inferred-fields
        }
      ]
    }
  },
  gender: {
    getList: {
      fields: ["value"],
    },
    getOne: {
      fields: ["value"],
    },
    // Listing and creating genders works.  However, editing and deleting
    // them does not work, as this table does not have "id" as it's primary key,
    // and this confuses Inferencer. However, note, the point of Inferencer is
    // not to be used in production, but rather to suggest code to start from.
    // The sugested code should be modifiable to use the correct primary key
    // for this table.
  },
  //
  // Below are from Sample app and do not relate to Toto database:
  blog_posts: {
    getList: {
      fields: [
        "id",
        "title",
        {
          category: ["id", "title"],
        },
        "category_id",
        "content",
        "created_at",
      ],
    },
    getOne: {
      fields: ["id", "title", "content", "category_id"],
    },
  },
  categories: {
    getList: {
      fields: ["id", "title", "created_at"],
    },
    default: {
      fields: ["id", "title"],
    },
  },
};
